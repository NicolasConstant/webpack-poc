var mySecondDep = require("./my-second-dep.js");

var scaleByHeight = function(a) {
    return a * mySecondDep.getHeight();
}

var exports = module.exports = { scaleByHeight };