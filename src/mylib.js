var myDep = require("./my-dep.js");

module.exports = {
    myFunction: function(a, b) {
        return a + myDep.scaleByHeight(b);
    }
}