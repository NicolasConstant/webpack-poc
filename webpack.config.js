const path = require('path');

module.exports = {
  entry: './src/mylib.js',
  mode: 'production',
  output: {
    filename: 'mylib.min.js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'var',
    library: 'MyLib'
  }
};