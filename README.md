# webpack-poc

Webpack minimal proof of concept

## Prerequisites 

You need npm to get webpack: [Node.js](https://nodejs.org/en/)

## Setup the project

Once npm is here, install webpack:

```
npm install webpack webpack-cli -g
```

## Run webpack

When webpack is installed, you just need to run webpack in the root folder (where the file webpack.config.js is located):

```
webpack
```

You should get a *mylib.min.js* file in the dist folder. 

(Warning: I have commited the file for easy consultation, delete it before to ensure the existing file is really the one you builded.)

## Test it

Validate it's working by running test.html.